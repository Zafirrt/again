extends Node

var random_arr = [0,1,2,3,4]
var states = []
var last_state = -1
var ind

const MAX_ARR_SIZE = 5

func shuffle_arr():
	randomize()
	random_arr.shuffle()

func get_random_state():
	if ind != MAX_ARR_SIZE:
		var state_val = random_arr[ind]
		ind += 1
		states.append(state_val)
		last_state = state_val
	Global.reload_scene()


func reload():
	ind = 0
	states = []
	shuffle_arr()

func _ready():
	reload()
