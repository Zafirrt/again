extends CanvasLayer

onready var animation_player = $AnimationPlayer

func _ready():
	$Control.hide()

func change_scene(new_scene, delay=0.5):
	$Control.show()
	yield(get_tree().create_timer(delay), "timeout")
	animation_player.play("Fade In")
	yield(animation_player, "animation_finished")
	get_tree().change_scene(new_scene)
	animation_player.play("Fade Out")
	yield(animation_player, "animation_finished")
	$Control.hide()
