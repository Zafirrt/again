extends AudioStreamPlayer

enum {MAINMENU, LEVEL1, LEVEL2, LEVEL3}

func play_music(music):
	match music:
		LEVEL1:
			stream = load("res://Assets/Sounds/Background Music/Level1.ogg")
		LEVEL2:
			stream = load("res://Assets/Sounds/Background Music/Level2.ogg")
		LEVEL3:
			stream = load("res://Assets/Sounds/Background Music/Level3.ogg")
		MAINMENU:
			stream = load("res://Assets/Sounds/Background Music/MainMenu.ogg")
	play()

func _ready():
	play_music(MusicPlayer.MAINMENU)
