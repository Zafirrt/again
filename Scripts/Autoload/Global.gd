extends Node

var player_at_flag = false
var level = "Level3"
var states = []
var last_medal_state = 0
var last_level

func reload():
	Global.last_medal_state = 0
	Global.last_level = ""
	State.reload()

func reload_scene():
	get_tree().reload_current_scene()
