extends CanvasLayer

func _on_Level_Select_pressed():
	get_tree().paused = false
	Global.last_level = Global.level
	queue_free()
	MusicPlayer.play_music(MusicPlayer.MAINMENU)
	SceneChanger.change_scene("res://Scenes/Level Select.tscn", 0)

func _on_Resume_pressed():
	get_tree().current_scene.handle_pause()
