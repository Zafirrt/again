extends Node

enum {NONE, NONE2, SHIRT, BRONZE, SILVER, GOLD, PLATINUM}

func _on_Level_Select_pressed():
	Global.last_level = Global.level
	queue_free()
	MusicPlayer.play_music(MusicPlayer.MAINMENU)
	SceneChanger.change_scene("res://Scenes/Level Select.tscn", 0)

func _on_Replay_and_Random_pressed():
	queue_free()
	State.get_random_state()
	
func _ready():
	if Global.last_medal_state == State.MAX_ARR_SIZE+1:
		get_node("MarginContainer/Background/VBoxContainer/HBoxContainer").remove_child(get_node("MarginContainer/Background/VBoxContainer/HBoxContainer/Replay and Random"))
	
