extends TextureButton


enum {NONE, NONE2, SHIRT, BRONZE, SILVER, GOLD, PLATINUM}
var medal: int

func _ready():
	pass # Replace with function body.

func set_medal(val):
	medal = max(medal, val)
	medal = min(medal, PLATINUM)

func set_medal_texture():
	match int(medal):
		NONE:
			pass
		NONE2:
			pass
		SHIRT:
			get_child(0).texture = load("res://Assets/Medals/I survived T-shirt.png")
		BRONZE:
			get_child(0).texture = load("res://Assets/Medals/Bronze.png")
		SILVER:
			get_child(0).texture = load("res://Assets/Medals/Silver.png")
		GOLD:
			get_child(0).texture = load("res://Assets/Medals/Gold.png")
		PLATINUM:
			get_child(0).texture = load("res://Assets/Medals/Platinum.png")

func save():
	var save_dict = {
		"path" : get_path(),
		"medal" : int(medal),
		"texture_normal" : texture_normal,
		"texture_hover" : texture_hover,
		"texture_pressed" : texture_pressed
	}
	return save_dict


func _on_TextureButton_pressed():
	get_tree().current_scene.get_node("AudioStreamPlayer").play()
	Global.level = name
	Global.last_medal_state = 0
	MusicPlayer.stop()
	SceneChanger.change_scene("res://Scenes/Levels/" + name + "_Base.tscn")
