extends Area2D

var timer

func state_up():	
	$CollisionShape2D.disabled = false
	$AnimatedSprite.play("Up")
	timer.wait_time = 0.5
	timer.start()
	yield(timer, "timeout")
	state_neutral()

func state_neutral():
	$AnimatedSprite.play("Neutral")
	timer.wait_time = 3.0
	timer.start()
	yield(timer, "timeout")
	state_down()
	
func state_down():
	$CollisionShape2D.disabled = true
	$AnimatedSprite.play("Down")
	timer.wait_time = 3.0
	timer.start()
	yield(timer, "timeout")
	state_up()

func _ready():
	timer = Timer.new()
	add_child(timer)
	timer.one_shot = true
	
	timer.wait_time = 1.0
	timer.start()
	yield(timer, "timeout")
	state_up()

func _on_Slime_Monster_body_entered(body):
	if body.get_name() == "Player":
		body.death()
		body.restart()
