extends Area2D


enum {DOCILE, AIM, AIMING, SHOOT, SHOOTING, DONE}

export (int) var ACCEL_X = 10
var state: int
var bomb: Node2D
var velocity = Vector2()
var timer
var my_position: int

func _physics_process(delta):
	match state:
		AIM:
			state = AIMING
			velocity.x = -10
			timer.wait_time = 2.0
			timer.start()
			yield(timer, "timeout")
			state = SHOOT
		AIMING:
			my_position = get_parent().get_node("Player").position.y
		SHOOT:
			position.x = 1400
			state = SHOOTING
		SHOOTING:
			velocity.x -= delta * ACCEL_X
			position.y = my_position
			position.x += velocity.x
			if global_position.x < -40:
				state = DONE
				velocity.x = 0
		DONE:
			state = AIM

func _ready():
	position.x = 1400
	timer = Timer.new()
	add_child(timer)
	timer.one_shot = true
	state = AIM

func _on_Bomb_body_entered(body):
	if body.get_name() == "Player" and not Global.player_at_flag:
		body.death()
		body.restart()

