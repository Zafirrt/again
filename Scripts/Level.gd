extends Node


var state;
var pause_node: Node

enum {PAUSED, UNPAUSED}
enum {SPIKES, MIRROR, UPSIDEDOWN, MONSTERS, BOMBS}

func add_state_to_current_scene():
	for i in State.states:
		match i:
			SPIKES:
				get_tree().current_scene.add_child(load("res://Scenes/Levels/" + Global.level + "_Spikes.tscn").instance())
			MIRROR:
				get_tree().current_scene.zoom.x = -1
			UPSIDEDOWN:
				get_tree().current_scene.zoom.y = -1
			MONSTERS:
				get_tree().current_scene.add_child(load("res://Scenes/Levels/" + Global.level + "_Monsters.tscn").instance())
			BOMBS:
				get_tree().current_scene.add_child(load("res://Scenes/Obstacle/Bomb.tscn").instance())

func handle_pause():
	if state == UNPAUSED:
		get_tree().paused = true
		pause_node = load("res://Scenes/Pause Menu.tscn").instance()
		get_tree().current_scene.add_child(pause_node)
		state = PAUSED
	else:
		get_tree().current_scene.remove_child(pause_node)
		get_tree().paused = false
		state = UNPAUSED

func _ready():
	state = UNPAUSED
	Global.player_at_flag = false
	add_state_to_current_scene()
	if(State.last_state != -1):
		if len(State.states) == 1:
			if Global.level == "Level1":
				MusicPlayer.play_music(MusicPlayer.LEVEL1)
			elif Global.level == "Level2":
				MusicPlayer.play_music(MusicPlayer.LEVEL2)
			elif Global.level == "Level3":
				MusicPlayer.play_music(MusicPlayer.LEVEL3)
		match State.last_state:
			SPIKES:
				get_tree().current_scene.add_child(load("res://Scenes/Random_Panels/Spikes_Panel.tscn").instance())
			MIRROR:
				get_tree().current_scene.add_child(load("res://Scenes/Random_Panels/Mirror_Panel.tscn").instance())
			UPSIDEDOWN:
				get_tree().current_scene.add_child(load("res://Scenes/Random_Panels/UpsideDown_Panel.tscn").instance())
			MONSTERS:
				get_tree().current_scene.add_child(load("res://Scenes/Random_Panels/Monsters_Panel.tscn").instance())
			BOMBS:
				get_tree().current_scene.add_child(load("res://Scenes/Random_Panels/Bombs_Panel.tscn").instance())
		State.last_state = -1
