extends Control

func _ready():
	load_level_state()
	var nodes = get_tree().get_nodes_in_group("Persist")
	for node in nodes:
		if node.name == Global.last_level:
			node.set_medal(Global.last_medal_state)
	save_level_state()
	load_level_state()
	Global.reload()

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		SceneChanger.change_scene("res://Scenes/Main Menu.tscn", 0)

func save_level_state():
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	for node in save_nodes:
		# Check the node is an instanced scene so it can be instanced again during load.
#		if node.filename.empty():
#			print("persistent node '%s' is not an instanced scene, skipped" % node.name)
#			continue

		# Check the node has a save function.
		if !node.has_method("save"):
			print("persistent node '%s' is missing a save() function, skipped" % node.name)
			continue

		# Call the node's save function.
		var node_data = node.call("save")

		# Store the save dictionary as a new line in the save file.
		save_game.store_line(to_json(node_data))
	save_game.close()

func load_level_state():
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
		return # Error! We don't have a save to load.

	# Load the file line by line and process that dictionary to restore
	# the object it represents.
	save_game.open("user://savegame.save", File.READ)
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())

		# Firstly, we need to create the object and add it to the tree and set its position.
		var new_object = get_node(node_data["path"])
		# Now we set the remaining variables.
		for i in node_data.keys():
			if i == "path":
				continue
			new_object.set(i, node_data[i])
		new_object.set_medal_texture()

	save_game.close()
