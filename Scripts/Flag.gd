extends Area2D

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		Global.player_at_flag = true
		Global.last_medal_state += 1
		get_tree().current_scene.add_child(load("res://Scenes/Finish Level.tscn").instance())
