extends KinematicBody2D

export (int) var speed = 300
export (int) var jump_speed = -500
export (int) var GRAVITY = 1000
export (int) var MAX_WALL_SLIDE_SPEED = 200
export (int) var WALL_SLIDE_ACCELERATION = 1

const UP = Vector2(0,-1)

var velocity = Vector2()
var can_double_jump = true
var flying = false
var dead = false
var slide_state = false
var in_air = false


func handle_jump():
	if is_on_floor():
		velocity.y = jump_speed
		flying = true
		in_air = true
	elif not is_on_floor() and can_double_jump:
		can_double_jump = false
		velocity.y = jump_speed
		flying = true
		in_air = true
		
func handle_horizontal_movement():
	if not Input.is_action_pressed('ui_right') and not Input.is_action_pressed('ui_left'):
		velocity.x = 0
	elif Input.is_action_pressed('ui_right') and not Input.is_action_pressed('ui_left'):
		velocity.x = speed
	elif Input.is_action_pressed('ui_left') and not Input.is_action_pressed('ui_right'):
		velocity.x = -speed


func handle_wall_movement():
	var wall_side = ''
	for i in range(get_slide_count()):
		var collision = get_slide_collision(i)
		if collision.normal.x > 0:
			wall_side = 'left'
			velocity.x = -speed
		elif collision.normal.x < 0:
			wall_side = 'right'
			velocity.x = speed

	if Input.is_action_pressed('ui_right') or Input.is_action_pressed('ui_left'):
		if velocity.y >= 0:
			velocity.y = min(velocity.y + WALL_SLIDE_ACCELERATION, MAX_WALL_SLIDE_SPEED)

	# Wall jump
	if Input.is_action_just_pressed("ui_up"):
		if wall_side == 'left' and Input.is_action_pressed('ui_right'):
			in_air = true
			velocity.y = jump_speed
			velocity.x = speed
			flying = true
		elif wall_side == 'right' and Input.is_action_pressed('ui_left'):
			in_air = true
			velocity.y = jump_speed
			velocity.x = -speed
			flying = true

func handle_movement():
	if is_on_wall() and not is_on_floor():
		can_double_jump = false
		handle_wall_movement()
	else:
		handle_horizontal_movement()
		if Input.is_action_just_pressed("ui_up"):
			handle_jump()

	if not Input.is_action_pressed("ui_up") and flying:
		flying = false
		velocity.y = max(velocity.y, 0)

func restart():
	yield(get_tree().create_timer(1.0), "timeout")
	dead = false
	Global.reload_scene()

func death():
	dead = true
	get_node("Sprite").set_visible(false)
	get_node("Death Particles").restart()

func _physics_process(delta):
	if not dead and not Global.player_at_flag:
		if Input.is_action_just_pressed("ui_cancel"):
			get_parent().handle_pause()
		if is_on_floor():
			can_double_jump = true
		velocity.y += delta * GRAVITY
		handle_movement()
		velocity = move_and_slide(velocity, UP)
		if (is_on_floor() or is_on_wall()) and in_air:
			in_air = false
			$SFX/Thump.play()
		for i in range(get_slide_count()):
			var collision = get_slide_collision(i)
			if collision.collider.get_name() == "Spikes":
				death()
				restart()
