extends Control

func _on_Quit_pressed():
	get_tree().quit()

func _on_Level_Select_Button_pressed():
	$AudioStreamPlayer.play()
	SceneChanger.change_scene("res://Scenes/Level Select.tscn", 0)


func _on_Delete_Save_File_pressed():
	$AudioStreamPlayer.play()
	yield(get_tree().create_timer(1.0), "timeout")
	var dir = Directory.new()
	dir.remove("user://savegame.save")
	$Save_Deleted_Message.show()
	yield(get_tree().create_timer(2.0), "timeout")
	$Save_Deleted_Message.hide()
	
	
